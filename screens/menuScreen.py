from config import pygame
import config
import screens.gameScreen as gameScreen


class MenuScreen:
    def __init__(self):
        self.screen = pygame.display.set_mode((config.SCREEN_WIDTH, config.SCREEN_HEIGHT), pygame.FULLSCREEN)

        self.font = pygame.font.Font(None, 36)

        self.items = [
            (self.__setMenuItemInCentre("Нова гра",-1)),
            (self.__setMenuItemInCentre("Налаштування",0)),
            (self.__setMenuItemInCentre("Вихід",1))
        ]


    def draw(self):
        self.screen.fill((0, 0, 0))
        for item in self.items:
            text_surface = self.font.render(item[0], True, (255, 255, 255))
            self.screen.blit(text_surface, item[1])


    def checkEvents(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:                        # Обробка подій натиснення клавіш миші
            self.__checkEventMouseButton(event)


    #
    #   Event methods
    # 
    def __checkEventMouseButton(self, event):
        for item in self.items:
            text_rect = self.font.render(item[0], True, (255, 255, 255)).get_rect(topleft=item[1])
            if text_rect.collidepoint(event.pos):
                if item[0] == "Нова гра":
                    config.SCREEN = gameScreen.GameScreen()
                    config.SCREEN_FOCUS = config.SCR_GAME
                elif item[0] == "Налаштування":
                    pass
                elif item[0] == "Вихід":
                    pygame.quit()
                    quit()


    # def __checkEventKeyDown(self, event): pass

    # def __checkEventKeyPressed(self, keys): pass



    #
    #   Secondary private logic
    # 
    def __setMenuItemInCentre(self, title, posMenuItem):
            textMargin = 75
            return (title,
                ((config.SCREEN_WIDTH - self.font.size(title)[0]) / 2,
                (config.SCREEN_HEIGHT / 2) + (posMenuItem * textMargin))
            )
