from config import pygame
import config

"""
Модуль містить інструменти розробки та тестування
"""

class DevTools:

    _SCREEN = None
    _MAP_WIDTH = 0
    _MAP_HEIGHT = 0

    # параметри корегування системи координат
    _CORRECTION_X = 0
    _CORRECTION_Y = 0

    def __init__(self, screen, mapWidth, mapHeight, correction_x, correction_y):
        DevTools._SCREEN = screen
        DevTools._MAP_WIDTH = mapWidth
        DevTools._MAP_HEIGHT = mapHeight
        DevTools._CORRECTION_X = correction_x
        DevTools._CORRECTION_Y = correction_y

        self._cursor = self._Cursor(self.__updateFixedPanel)
        self._mapCentre = self._MapCentre()
        self._fixedPanel = self._FixedPanel(self._cursor.x, self._cursor.y)

    def drawBackTools(self, shift_x, shift_y):
        self._mapCentre.draw(shift_x, shift_y)
        self._cursor.draw(shift_x, shift_y)

    def drawFrontTool(self, shift_x, shift_y):
        self._fixedPanel.draw()

    def checkEvents(self, event):
        self._cursor.checkEvents(event)

    def __updateFixedPanel(self, cursor_x, cursor_y):
        self._fixedPanel.titleCursor_X = cursor_x
        self._fixedPanel.titleCursor_Y = cursor_y

    class _Cursor():
        def __init__(self, funcUpdatePanel):
           self.x = 400
           self.y = 400
           self.width = 50
           self.height = 50
           self.color = (255, 0, 0)
           self.step = 10
           self.ministep = 1
           
           self.funcUpdatePanel = funcUpdatePanel

        def draw(self, shift_x, shift_y):
            obj = pygame.Rect(self.x + shift_x + DevTools._CORRECTION_X,  self.y + shift_y + DevTools._CORRECTION_Y, self.width, self.height)
            pygame.draw.rect(DevTools._SCREEN, self.color, obj)

        def checkEvents(self, event):
            # Обробка подій утримання кнопок
            keys = pygame.key.get_pressed()

            if   keys[pygame.K_w]: self.__moveY(-self.step)
            elif keys[pygame.K_s]: self.__moveY(self.step)
            elif keys[pygame.K_a]: self.__moveX(-self.step)
            elif keys[pygame.K_d]: self.__moveX(self.step)

            # Обробка подій натиснення кнопки
            if event.type == pygame.KEYDOWN:                        
                if event.key == pygame.K_UP: self.__moveY(-self.ministep)
                elif event.key == pygame.K_DOWN: self.__moveY(self.ministep)
                elif event.key == pygame.K_LEFT: self.__moveX(-self.ministep)
                elif event.key == pygame.K_RIGHT: self.__moveX(self.ministep)


        def __moveX(self, step):
            new_x = self.x + step
            limit_x = DevTools._MAP_WIDTH/2
            if abs(new_x)  < limit_x and abs(new_x + self.width) < limit_x:
                self.x = new_x
                self.funcUpdatePanel(self.x, self.y)

        def __moveY(self, step):
            new_y = self.y + step
            limit_y = DevTools._MAP_HEIGHT/2
            if abs(new_y) < limit_y and abs(new_y + self.height) < limit_y:
                self.y = new_y
                self.funcUpdatePanel(self.x, self.y)


    class _MapCentre:
        def __init__(self):
            self.width = 100
            self.height = 100
            self.x = DevTools._CORRECTION_X - (self.width/2)
            self.y = DevTools._CORRECTION_Y - (self.height/2)
            self.color1 = (255, 255, 0)
            self.color2 = (255, 100, 0)
            self.color3 = (100, 0, 30)
            self.color4 = (0, 109, 190)
            self.surface = self.__generateSurface()

        def draw(self, shift_x, shift_y):
            DevTools._SCREEN.blit(self.surface, (self.x + shift_x,  self.y + shift_y))

        def __generateSurface(self):
            surface = pygame.Surface((self.width, self.height), pygame.SRCALPHA)
            pygame.draw.rect(surface, self.color1, (0, 0, self.width/2, self.height/2))
            pygame.draw.rect(surface, self.color2, (self.width/2, 0, self.width/2, self.height/2))
            pygame.draw.rect(surface, self.color3, (0,  self.height/2, self.width/2, self.height/2))
            pygame.draw.rect(surface, self.color4, (self.width/2,  self.height/2, self.width/2, self.height/2))
            return surface
        

    class _FixedPanel:
        def __init__(self, cursor_x, cursor_y):
            self.width = 600
            self.height = 100
            self.x = 1300
            self.y = config.SCREEN_HEIGHT - self.height
            self.color = (140, 140, 140)

            self.titleFont = pygame.font.SysFont("Times New Roman", 24)
            self.titleColor = (255, 255, 255)

            self.titleCursor = "Координати курсора: ("
            self.titleCursor_X = cursor_x
            self.titleCursor_Y = cursor_y

            self.titlePlanet = "Планета: "
            self.titlePlanetName = ""
            self.titlePlanet_X = ""
            self.titlePlanet_Y = ""

        def draw(self):
            surface = pygame.Surface((self.width, self.height), pygame.SRCALPHA)
            surface.fill(self.color)
            curcorCoord_title = self.titleFont.render(self.__getTitleCursor(), True, self.titleColor)
            surface.blit(curcorCoord_title, (10, 10))

            planet_title = self.titleFont.render(self.__getTitlePlanet(), True, self.titleColor)
            surface.blit(planet_title, (10, 40))
            DevTools._SCREEN.blit(surface, (self.x, self.y))

        def setPlanetData(self, title, x, y):
            self.titlePlanetName = title
            self.titlePlanet_X = str(x)
            self.titlePlanet_Y = str(y)

        
        def __getTitleCursor(self):
            return (self.titleCursor + str(self.titleCursor_X) + ", " + 
                    str(self.titleCursor_Y) + ")"
                )
        
        def __getTitlePlanet(self):
            return (self.titlePlanet + self.titlePlanetName + " (" + self.titlePlanet_X + ", " + self.titlePlanet_Y + ")")
