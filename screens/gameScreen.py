from config import pygame
import config
import screens.menuScreen as menuScreen
import screens.map.Map as Map
import screens.DevTools as DevTools

class GameScreen:
    def __init__(self):
        self.screen = pygame.display.set_mode((config.SCREEN_WIDTH, config.SCREEN_HEIGHT), pygame.FULLSCREEN)

        # створення мапи (основного ігрового поля)
        self.Map = Map.Map(self.screen)

        # інструменти розробки
        config.DEV_TOOLS = DevTools.DevTools(self.screen, self.Map.width, self.Map.height, self.Map.correct_X, self.Map.correct_Y)


    def draw(self):
        self.screen.fill((0, 0, 0))

        # відображення інструментів розробки (фонових)
        config.DEV_TOOLS.drawBackTools(self.Map.shift_X, self.Map.shift_Y)

        # відображення мапи (основного ігрового поля)
        self.Map.draw()

        # відображення інструментів розробки (передніх)
        config.DEV_TOOLS.drawFrontTool(self.Map.shift_X, self.Map.shift_Y)
        

    def checkEvents(self, event):
        # Обробка подій натиснення кнопки
        if event.type == pygame.KEYDOWN:
            self.__checkEventKeyDown(event)

        # відловка подій для інструментів розробки
        config.DEV_TOOLS.checkEvents(event)

        # відловка подій для мапи (основного ігрового поля)
        self.Map.checkEvents(event)


    def __checkEventKeyDown(self, event):
        if event.key == pygame.K_ESCAPE:
            config.SCREEN = menuScreen.MenuScreen()
            config.SCREEN_FOCUS = config.SCR_MENU
