from config import pygame


class Minimap:
    def __init__(self, screen, planets):
        self.screen = screen
        self.width = 560
        self.height = 400
        
        self.shiftAxis_X = self.width / 2
        self.shiftAxis_Y = self.height / 2

        self.x = 100
        self.y = 600
        self.reductionRatio = 4 / 100
        self.colorBackground = (30, 30, 30)
        self.colorCentrePoint = (255,255,0)
        self.colorMiniPlanet = (255,0,0)
        self.surface = self.__generateSurface(planets)

    def draw(self):
        self.screen.blit(self.surface, (self.x, self.y))

    def __generateSurface(self, planets):
        surface = pygame.Surface((self.width+50, self.height+50), pygame.SRCALPHA)
        surface.fill(self.colorBackground)
        pygame.draw.circle(surface, self.colorCentrePoint, (self.shiftAxis_X+25, self.shiftAxis_Y+25), 5)
        for planet in planets:
            pygame.draw.circle(surface, self.colorMiniPlanet, 
                               self.__getMiniCoords(planet.x, planet.y), 
                               self.__getMiniRadius(planet.radius))
        return surface
    
    def __getMiniCoords(self, x, y):
        miniX = (x * self.reductionRatio) + self.shiftAxis_X
        miniY = (y * self.reductionRatio) + self.shiftAxis_Y
        return (miniX, miniY)

    def __getMiniRadius(self, radius):
        if radius >= 60:
            return 5
        elif radius >= 45:
            return 4
        else:
            return 2