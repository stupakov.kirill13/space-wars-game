from config import pygame
import config
import random
from screens.map.PlanetNames import PLANET_NAMES

"""
Модуль відповідає за створення планет.
"""

class Planets:

    _START_SHIFT_X = 0
    _START_SHIFT_Y = 0
    _WIDTH_MAP = 0
    _HEIGHT_MAP = 0

    def __init__(self, screen, widthMap, heightMap, correct_X, correct_Y):
        self._screen = screen
        Planets._START_SHIFT_X = correct_X
        Planets._START_SHIFT_Y = correct_Y
        Planets._WIDTH_MAP = widthMap
        Planets._HEIGHT_MAP = heightMap
        self._Planet._PLANET_POS = []

        self.numbPlanets = 500
        self.planets = []
        for i in range(self.numbPlanets):
            self.planets.append(self._Planet(i))

    def draw(self, shift_X, shift_Y):
        for i in range(self.numbPlanets):
            self._screen.blit(self.planets[i].surface, (self.planets[i].x + shift_X, self.planets[i].y + shift_Y))
            self.planets[i].updateCliclArea(shift_X, shift_Y)

    def checkEvents(self, event):
        for i in range(self.numbPlanets):
            self.planets[i].checkEvents(event)


    class _Planet:
        
        _IMG_PATH = "src/img/planets/planet_"

        _TITLE_FONT = pygame.font.Font(None, 24)
        _TITLE_COLOR = (0, 255, 0)
        _TITLE_TOP_PADDING = 10

        _PLANET_POS = []

        def __init__(self, i):
            self.radius = random.randint(25,70)
            self.diameter = self.radius*2
            self.title = PLANET_NAMES[i]

            self.titleWidth = self._TITLE_FONT.size(self.title)[0]
            self.titleHeight = self._TITLE_FONT.size(self.title)[1]

            self.surfaceWidth = self.__getWidthSurface()
            self.surfaceHeight = self.diameter + self._TITLE_TOP_PADDING + self.titleHeight
            self.imgPadding = (self.surfaceWidth - self.diameter)/2
            self.titlePadding = (self.surfaceWidth - self.titleWidth)/2

            self.img = self._IMG_PATH + str(random.randint(1, 27)) + ".png"

            self.x, self.y = self.__generateCoord()

            self.surface = self.__generateSurface()
            self.surfaceClickArea = None


        def updateCliclArea(self, shift_X, shift_Y):
            self.surfaceClickArea = pygame.Rect(self.x + shift_X + self.imgPadding, self.y + shift_Y, self.diameter, self.diameter)


        def checkEvents(self, event):
            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                if self.surfaceClickArea.collidepoint(mouse_pos) and event.button == 1:
                    config.DEV_TOOLS._fixedPanel.setPlanetData(self.title, 
                                                               self.x + self.radius - Planets._START_SHIFT_X + self.imgPadding, 
                                                               self.y + self.radius - Planets._START_SHIFT_Y)

        def __generateCoord(self):
            while True:
                x = random.randint(-Planets._WIDTH_MAP/2, (Planets._WIDTH_MAP/2) - self.diameter) + Planets._START_SHIFT_X
                y = random.randint(-Planets._HEIGHT_MAP/2, (Planets._HEIGHT_MAP/2) - self.diameter) + Planets._START_SHIFT_Y

                planet_rect = pygame.Rect(x + self.imgPadding, y, self.diameter+200, self.diameter+200)

                if not any(planet_rect.colliderect(p) for p in self._PLANET_POS):
                    self._PLANET_POS.append(planet_rect)
                    return x, y
            

        def __generateSurface(self):
            backgroundColor = (255,0,0)

            circleBackgroundColor = (0,0,0)

            surface = pygame.Surface((self.surfaceWidth, self.surfaceHeight), pygame.SRCALPHA)
            #surface.fill(backgroundColor)

            pygame.draw.circle(surface, circleBackgroundColor, (self.surfaceWidth/2, self.radius), self.radius)

            image = pygame.image.load(self.img)
            image = pygame.transform.scale(image, (self.diameter, self.diameter))

            planet_title = self._TITLE_FONT.render(self.title, True, self._TITLE_COLOR)

            surface.blit(image, (self.imgPadding, 0))
            surface.blit(planet_title, (self.titlePadding, self.diameter + self._TITLE_TOP_PADDING))

            return surface
        
        def __getWidthSurface(self):
            if self.titleWidth > self.diameter:
                return self.titleWidth
            else:
                return self.diameter

