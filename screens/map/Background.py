from config import pygame
import random
import math

"""
Модуль містить елементи фону мапи.
"""

class Background:

    _SCREEN = None
    _MAP_WIDTH = 0
    _MAP_HEIGHT = 0
    _CORRECT_X = 0
    _CORRECT_Y = 0

    def __init__(self, screen, mapWidth, mapHeight, correct_X, correct_Y):
        Background._SCREEN = screen
        Background._MAP_WIDTH = mapWidth
        Background._MAP_HEIGHT = mapHeight
        Background._CORRECT_X = correct_X
        Background._CORRECT_Y = correct_Y

        self.numbStars = 20000
        self.stars = []
        for i in range(self.numbStars):
            self.stars.append(self._Star())

    def draw(self, shift_X, shift_Y):
        for star in self.stars:
            star.draw(shift_X, shift_Y)

    class _Star:

        _COLORS = [(255, 229, 249), 
                   (216, 230, 200),
                   (167, 211, 211),
                   (226, 228, 195),
                   (198, 232, 225),
                   (148, 114, 148),
                   (167, 154, 148),
                   (163, 179, 172),
                   (189, 179, 150),
                   (183, 192, 212),
                   (247, 198, 216),
                   (154, 176, 184)]
        
        _FADING = [
                [
                    random.randint(80, 150),
                    random.randint(150, 230)
                ],
                [
                    random.randint(80, 120),
                    random.randint(120, 150),
                    random.randint(150, 180),
                    random.randint(180, 230)
                ]
            ]
        
        _IGNITION_TIME = 20
        _GLOW_TIME = 30

        def __init__(self):
            self.x = random.randint(-Background._MAP_WIDTH/2, Background._MAP_WIDTH/2) + Background._CORRECT_X
            self.y = random.randint(-Background._MAP_HEIGHT/2, Background._MAP_HEIGHT/2) + Background._CORRECT_Y
            self.color = self._COLORS[random.randint(0, len(self._COLORS)-1)]
            self.size = random.choice([1, 3, 5])
            self.surface = self.__generateSurface()
            self.shiftCoef = random.choice([2, 6, 10])

            self.flash = False
            self.flashStep = 0

            self.flashStatus = 1    # 1  -  ignition
                                    # 2  -  glow
                                    # 3  -  fading 

            self.surfaceFlash =  pygame.Surface((self.size*5, self.size*5), pygame.SRCALPHA)
            self.centreFlash = math.floor(self.size / 2) + (self.size * 2)
            self.ignitionTime = self._IGNITION_TIME
            self.glowTime = self._GLOW_TIME


        def draw(self, shift_X, shift_Y):
            Background._SCREEN.blit(self.surface, (self.x + (shift_X/self.shiftCoef), self.y + (shift_Y/self.shiftCoef)))
            self.__drawFlashingStar(shift_X, shift_Y)


        def __drawFlashingStar(self, shift_X, shift_Y):
            if self.size == 5 and self.flash == False and random.randint(1, 25000) == 1:
                self.flash = True

            if self.flash == True:
                Background._SCREEN.blit(self.surfaceFlash, (self.x - (self.size*2) + (shift_X/self.shiftCoef), self.y - (self.size*2) + (shift_Y/self.shiftCoef)))
                self.__flashMechanics()


        def __flashMechanics(self):
            if self.flashStatus == 1:
                if self.ignitionTime == 0:
                    self.ignitionTime = self._IGNITION_TIME
                    self.__generateFlash()
                    self.flashStep = self.flashStep + 1
                else:
                    self.ignitionTime = self.ignitionTime - 1
                if self.flashStep + 1 == self.centreFlash:
                    self.flashStatus = 2
            elif self.flashStatus == 2:
                if self.glowTime == 0:
                    self.glowTime = self._GLOW_TIME
                    self.flashStatus = 3
                else:
                    self.glowTime = self.glowTime - 1
            else:
                if self.ignitionTime == 0:
                    self.ignitionTime = self._IGNITION_TIME*2
                    self.__generateFlash()
                    self.flashStep = self.flashStep - 1
                else:
                    self.ignitionTime = self.ignitionTime - 1
                if self.flashStep == -1:
                    self.flashStatus = 1
                    self.flashStep = 0
                    self.flash = False


        def __generateFlash(self):
            self.surfaceFlash =  pygame.Surface((self.size*5, self.size*5), pygame.SRCALPHA)
            pygame.draw.rect(self.surfaceFlash, self.color, (self.centreFlash, self.centreFlash, 1, 1))
            
            for i in range(1, self.flashStep+2):
                tempColor = self.__getFadingColor(i * 230 / self.centreFlash)
                pygame.draw.rect(self.surfaceFlash, tempColor, (self.centreFlash, self.centreFlash-i, 1, 1))
                pygame.draw.rect(self.surfaceFlash, tempColor, (self.centreFlash, self.centreFlash+i, 1, 1))
                pygame.draw.rect(self.surfaceFlash, tempColor, (self.centreFlash-i, self.centreFlash, 1, 1))
                pygame.draw.rect(self.surfaceFlash, tempColor, (self.centreFlash+i, self.centreFlash, 1, 1))
            

        def __getFadingColor(self, fadingCoef):

            newColor = []
            for i in range(3):
                col = self.color[i] - fadingCoef
                if col < 0:
                    col = 0
                newColor.append(col)
            
            return newColor


        def __generateSurface(self):
            surface = pygame.Surface((self.size, self.size), pygame.SRCALPHA)
            if self.size == 3:
                surface = self.__genStarType_1(surface)
            elif self.size == 5:
                surface = self.__genStarType_2(surface)
            else:
                pygame.draw.rect(surface, self.color, (0,  0, self.size, self.size))
            return surface
            

        def __genStarType_1(self, surface):
            
            """
                Star type_1 (3x3).

                    Structure:
                  +-----------+
                  | 2   1   2 |
                  | 1   0   1 |
                  | 2   1   2 |
                  +-----------+

            """
            pygame.draw.rect(surface, self.color, (1, 1, 1, 1))

            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[0][0]), (0, 1, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[0][0]), (1, 0, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[0][0]), (1, 2, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[0][0]), (2, 1, 1, 1))

            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[0][1]), (0, 0, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[0][1]), (0, 2, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[0][1]), (2, 0, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[0][1]), (2, 2, 1, 1))

            return surface


        def __genStarType_2(self, surface):
            
            """
                Star type_2 (5x5).

                    Structure:
                  +-------------------+
                  |     4   3   4     |
                  | 4   2   1   2   4 |
                  | 3   1   0   1   3 |
                  | 4   2   1   2   4 |
                  |     4   3   4     | 
                  +-------------------+

            """
            pygame.draw.rect(surface, self.color, (2, 2, 1, 1))

            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][0]), (1, 2, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][0]), (2, 1, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][0]), (2, 3, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][0]), (3, 2, 1, 1))
        
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][1]), (1, 1, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][1]), (1, 3, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][1]), (3, 1, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][1]), (3, 3, 1, 1))

            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][2]), (0, 2, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][2]), (2, 0, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][2]), (4, 2, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][2]), (2, 4, 1, 1))

            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][3]), (0, 1, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][3]), (1, 0, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][3]), (0, 3, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][3]), (1, 4, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][3]), (3, 0, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][3]), (4, 1, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][3]), (3, 4, 1, 1))
            pygame.draw.rect(surface, self.__getFadingColor(self._FADING[1][3]), (4, 3, 1, 1))

            return surface
            
