from config import pygame
import config
import screens.map.Planets as planets
import screens.map.Background as background
import screens.map.Minimap as minimap

class Map:
    def __init__(self, screen):
        self.screen = screen
        self.width = 14000
        self.height = 10000

        # параметри корегування системи координат
        self.correct_X = config.SCREEN_WIDTH / 2 
        self.correct_Y = config.SCREEN_HEIGHT / 2

        # параметри камери гравця
        self.start_pos = None   # зсув миші
        self.dragging = False   # режим переміщення (true - так, false - ні)
        self.shift_X = 0        # значення, на яке треба зміщувати об'єкти за Х
        self.shift_Y = 0        # значення, на яке треба зміщувати об'єкти за У

        # створення фону з зірками
        self.background = background.Background(self.screen, self.width, self.height, self.correct_X, self.correct_Y)

        # створення планет
        self.planets = planets.Planets(self.screen, self.width, self.height, self.correct_X, self.correct_Y)

        # створення мінікарти
        self.minimap = minimap.Minimap(self.screen, self.planets.planets)

    def draw(self):
        # відображення фону з зірками
        self.background.draw(self.shift_X, self.shift_Y)

        # відображення планет
        self.planets.draw(self.shift_X, self.shift_Y)

        # відображення мінікарти
        self.minimap.draw()


    def checkEvents(self, event):
        self.__checkEventsCombinations(event)                   # Оброка комбінацій різних подій

        # відловка подій для планет
        self.planets.checkEvents(event)

    def __checkEventsCombinations(self, event):
        #  перетягування мапи за допомогою миші
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            self.dragging = True
            self.start_pos = pygame.mouse.get_pos()
        elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            self.dragging = False
        elif event.type == pygame.MOUSEMOTION and self.dragging:
            self.__updateShiftMap()

    def __updateShiftMap(self):
        current_pos = pygame.mouse.get_pos()
        newShift_X = self.shift_X + current_pos[0] - self.start_pos[0]
        newShift_Y = self.shift_Y + current_pos[1] - self.start_pos[1]

        # правильна формула створення меж мапи (треба враховувати зміщення центру координат)
        if newShift_X> (- self.width/2) + self.correct_X  and newShift_X< (self.width/2) - self.correct_X:
            self.shift_X = newShift_X
        if newShift_Y> (- self.height/2) + self.correct_Y  and newShift_Y< (self.height/2) - self.correct_Y:
            self.shift_Y = newShift_Y
        self.start_pos = current_pos