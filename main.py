from config import pygame
import config
import screens.menuScreen as menuScreen

screen = pygame.display.Info()
config.SCREEN_WIDTH = screen.current_w
config.SCREEN_HEIGHT = screen.current_h
config.SCREEN = menuScreen.MenuScreen()

while True:

    # Малювання вікна
    config.SCREEN.draw()

    pygame.display.update()


    for event in pygame.event.get():

        config.SCREEN.checkEvents(event)

        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
